FROM ubuntu:20.10

ADD texlive.profile http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz https://github.com/jgm/pandoc/releases/download/2.12/pandoc-2.12-1-amd64.deb https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.10.0/pandoc-crossref-Linux.tar.xz ./

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    git \
    wget \
    curl \
    unzip \
    libgmp10 \
    xzdec \
    xz-utils \
    gpg \
    fontconfig \
    util-linux \
    perl \
    libwww-perl \
    && dpkg -i pandoc-2.12-1-amd64.deb \
    && rm pandoc-2.12-1-amd64.deb \
    && tar xf pandoc-crossref-Linux.tar.xz \
    && chmod +x pandoc-crossref \
    && mv pandoc-crossref /usr/local/bin \
    && apt-get autoclean -y \
    && apt-get autoremove -y \
    && tar xzf install-tl-unx.tar.gz && cd install-tl-*/ && yes I |./install-tl -profile ../texlive.profile && cd .. && rm -rf texlive.profile install-tl* pandoc-*

RUN tlmgr init-usertree & tlmgr install picture multirow letltxmacro zref pgfplots pgfopts inlinedef fvextra enumitem siunitx titlesec titling circuitikz needspace mdframed newlfm lastpage glossaries beamertheme-focus fontaxes csquotes physics mweights fira appendixnumberbeamer mfirstuc algorithm2e ifoddpage relsize glossaries-extra adjustbox xfor datatool substr collectbox marvosym textpos supertabular tabulary tcolorbox environ trimspaces forest \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /usr/share/doc