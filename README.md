# Pandoc Docker
This is a Docker container that includes the entire TeX Live distribution and
pandoc. Currently it contains TeX Live 2019 and pandoc 2.9.1.1.

This container is based on Ubuntu 18.04.